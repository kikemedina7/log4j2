import com.utils.UtilesLog;
import enums.properties.test.bussiness.TipoLog;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        System.setProperty("my.log", "C:\\tmp\\pepe.log");
        new Main().init();

    }

    public void init (){
        Date firstDay = java.util.Calendar.getInstance().getTime();
        //log.debug(mensaje);
        UtilesLog.registrar(TipoLog.class,TipoLog.DEBUG,"Prueba 1"+new SimpleDateFormat("dd/MM/yyy").format(firstDay),"pepe");
        UtilesLog.registrar(TipoLog.class,TipoLog.DEBUG,"Prueba 2"+new SimpleDateFormat("dd/MM/yyy").format(firstDay),"cr7");
        UtilesLog.registrar(TipoLog.class,TipoLog.DEBUG,"Prueba 3"+new SimpleDateFormat("dd/MM/yyy").format(firstDay),"jr10");
    }
}

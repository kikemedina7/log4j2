package enums.properties.test.bussiness;

public enum TipoLog {
    DEBUG, ERROR, FATAL, INFO, WARNING
}
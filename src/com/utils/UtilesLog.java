package com.utils;

import enums.properties.test.bussiness.TipoLog;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;

public class UtilesLog {

    private static Logger log = Logger.getLogger(UtilesLog.class);


    @SuppressWarnings("rawtypes")
    public static void registrar(Class clase, TipoLog tipo, String mensaje, String nombreLog) {
        System.out.println(clase.getCanonicalName());
        RollingFileAppender r =(RollingFileAppender) Logger.getRootLogger().getAppender("File");
        r.setFile("C:\\tmp\\"+nombreLog+".log");
        System.out.println("C:\\tmp\\"+nombreLog+".log");
        r.activateOptions();
        Logger.getRootLogger().addAppender(r);
        log = LogManager.getLogger(clase);

        switch (tipo) {
            case DEBUG:
                log.debug(mensaje);
                break;
            case ERROR:
                log.error(mensaje);
                break;
            case FATAL:
                log.fatal(mensaje);
                break;
            case INFO:
                log.info(mensaje);
                break;
            case WARNING:
                log.warn(mensaje);
        }
    }

}